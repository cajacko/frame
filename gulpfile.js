'use strict';
 
var browserify 	= require('browserify'),
	gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	rename 		= require('gulp-rename'),
	minifyCss 	= require('gulp-minify-css'),
	uglify 		= require('gulp-uglify'),
	concat 		= require('gulp-concat'),
	rjs 		= require('gulp-requirejs'),
	source 		= require('vinyl-source-stream'),
	del 		= require('del'),
	fs 			= require('fs');

var sassImport = 'styles/import.scss';
var cssPath = 'public/styles/';
var jsPath = 'public/scripts/';
var fontPath = 'public/fonts/';

var normalizeCssPath = './node_modules/normalize.css/normalize.css';
var fontAwesomeCssPath = './node_modules/font-awesome/css/font-awesome.css';
var jqueryPath = './node_modules/jquery/dist//jquery.js';

/********************************************************
* DEFINE PROJECTS AND THEIR PATHS						*
********************************************************/

var projects = {
	rememberwhen: './projects/remember_when/',
	photine: './projects/photine/website/'
	// frame: './dist/frame/',
	// ninestudios: './projects/nine_studios/'
};

var projectPath = './website/';
var projectCssPath = projectPath + cssPath;
var projectJsPath = projectPath + jsPath;

/********************************************************
* SASS													*
********************************************************/

gulp.task('sass', function() {
	return gulp.src(projectPath + sassImport)
		.pipe(sass().on('error', sass.logError))
		.pipe(rename('style.css'))
		.pipe(gulp.dest(projectCssPath))
		.pipe(rename('style.min.css'))
		.pipe(minifyCss())
		.pipe(gulp.dest(projectCssPath));
});

/********************************************************
* LIBRARY CSS											*
********************************************************/

gulp.task('libcss', function () {
	return gulp.src([normalizeCssPath, fontAwesomeCssPath])
		.pipe(concat('lib.css'))
		.pipe(rename('lib.css'))
		.pipe(gulp.dest(projectCssPath))
		.pipe(rename('lib.min.css'))
		.pipe(minifyCss())
		.pipe(gulp.dest(projectCssPath));
});

/********************************************************
* SCRIPTS 												*
********************************************************/

gulp.task('scripts', function() {
	var projectScriptDir = projectPath + 'scripts/';

	fs.readFile(projectScriptDir + 'import.json', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}

		data = JSON.parse(data)["import"];

		var jsImport = [];

		for(var i in data) {
			var path = data[i];

			var res = path.split("/");

			if(res.length > 1) {
				res[res.length - 1] = '_' + res[res.length - 1];

				path = res.join("/");
			} else {
				path = '_' + path;
			}

			jsImport.push(projectScriptDir + path + '.js');
		}
	
		return gulp.src(jsImport)
			.pipe(concat('script.js'))
			.pipe(gulp.dest(projectJsPath))
			.pipe(rename('script.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest(projectJsPath));
	});
});

/********************************************************
* LIBRARY SCRIPTS										*
********************************************************/

gulp.task('libscripts', function() {
	return gulp.src(jqueryPath)
		.pipe(concat('lib.js'))
		.pipe(gulp.dest(projectJsPath))
		.pipe(rename('lib.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(projectJsPath));
});

/********************************************************
* FONT AWESOME											*
********************************************************/

gulp.task('fonts', function() {
	gulp.src('./node_modules/font-awesome/fonts/*')
		.pipe(gulp.dest(projectPath + fontPath));

	gulp.src('./global/fonts/*')
		.pipe(gulp.dest(projectPath + fontPath));
});

/********************************************************
* WATCH TASKS											*
********************************************************/

gulp.task('watch', function () {
	gulp.watch(['./global/**/*.scss', projectPath + '**/*.scss'], ['sass']);
	gulp.watch(['./global/**/*.js', projectPath +  '**/*.js'], ['scripts']);
});




var framePath = './frame/';
var distPath = './dist/';
var projectCssPath = distPath + cssPath;
var projectJsPath = distPath + jsPath;

/********************************************************
* SASS													*
********************************************************/

gulp.task('frame:sass', function() {
	return gulp.src(distPath + sassImport)
		.pipe(sass().on('error', sass.logError))
		.pipe(rename('style.css'))
		.pipe(gulp.dest(projectCssPath))
		.pipe(rename('style.min.css'))
		.pipe(minifyCss())
		.pipe(gulp.dest(projectCssPath));
});

/********************************************************
* LIBRARY CSS											*
********************************************************/

gulp.task('frame:libcss', function () {
	return gulp.src([normalizeCssPath, fontAwesomeCssPath])
		.pipe(concat('lib.css'))
		.pipe(rename('lib.css'))
		.pipe(gulp.dest(projectCssPath))
		.pipe(rename('lib.min.css'))
		.pipe(minifyCss())
		.pipe(gulp.dest(projectCssPath));
});

/********************************************************
* SCRIPTS 												*
********************************************************/

gulp.task('frame:scripts', function() {
	var projectScriptDir = projectPath + 'scripts/';

	fs.readFile(projectScriptDir + 'import.json', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}

		data = JSON.parse(data)["import"];

		var jsImport = [];

		for(var i in data) {
			var path = data[i];

			var res = path.split("/");

			if(res.length > 1) {
				res[res.length - 1] = '_' + res[res.length - 1];

				path = res.join("/");
			} else {
				path = '_' + path;
			}

			jsImport.push(projectScriptDir + path + '.js');
		}
	
		return gulp.src(jsImport)
			.pipe(concat('script.js'))
			.pipe(gulp.dest(projectJsPath))
			.pipe(rename('script.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest(projectJsPath));
	});
});

/********************************************************
* LIBRARY SCRIPTS										*
********************************************************/

gulp.task('frame:libscripts', function() {
	return gulp.src(jqueryPath)
		.pipe(concat('lib.js'))
		.pipe(gulp.dest(projectJsPath))
		.pipe(rename('lib.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(projectJsPath));
});

/********************************************************
* FONT AWESOME											*
********************************************************/

gulp.task('frame:fonts', function() {
	gulp.src('./node_modules/font-awesome/fonts/*')
		.pipe(gulp.dest(projectPath + fontPath));

	gulp.src('./global/fonts/*')
		.pipe(gulp.dest(projectPath + fontPath));
});

/********************************************************
* BUILD PHOTINE FRAME									*
********************************************************/

gulp.task('frame:delete', function (cb) {
	del([distPath + '*'], cb()); // Delete existing build
});

var importScripts = [];

gulp.task('frame:copy', function () {
	/**
	 * Get the frame import file and import every 
	 * file into the distribution folder
	 */
	fs.readFile(framePath + 'import.json', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}

		data = JSON.parse(data);

		getFilePaths(data, '', '', '', '');

		for(var i in importScripts) {
			var path = importScripts[i].path;
			var name = '';
			var dest = distPath + importScripts[i].relativePath;

			if(importScripts[i].parent == 'scripts' || importScripts[i].parent == 'styles' || importScripts[i].parent == 'classes' || importScripts[i].parent == 'includes') {
				path += '_';
				name += '_';
			}

			if(importScripts[i].scope == 'global') {
				name += 'global_';
			} 

			name += importScripts[i].fileName + '.';
			path += importScripts[i].fileName + '.';

			if(importScripts[i].parent == 'scripts') {
				path += 'js';
				name += 'js';
			} else if(importScripts[i].parent == 'styles') {
				path += 'scss';
				name += 'scss';
			} else {
				path += 'php';
				name += 'php';
			}

			path = './website' + path;

			gulp.src(path).pipe(rename(name)).pipe(gulp.dest(dest));
		}
	});

	gulp.src([framePath + '**/*', framePath + '**/.*']).pipe(gulp.dest(distPath)); // Add all files from the frame directory

	del([distPath + 'dev_config.json']); // Delete existing build
});

function getFilePaths(data, path, parent, relativePath, scope) {
	if(typeof data === 'object') {
		for(var i in data) {

			var path2 = i;
			var path3 = path;
			var path4 = relativePath;

			if(i == 'global' || i == 'website') {
				var path5 = i;
			} else {
				var path5 = scope;
			}

			if(i == 'global') {
				path3 = '.' + path3;
			} else if(i == 'website') {
				path3 = projects.photine.slice(0, -1) + path3;
			} else if(!isNaN(i)) {
				path2 = '';
			} else {
				parent = i;
				if(path4 == '') {
					path4 = path2;
				} else {
					path4 = path4 + '/' + path2;
				}	
			}

			if(i == 'website') {
				path2 = path3;
			} else {
				path2 = path3 + '/' + path2;
			}


			getFilePaths(data[i], path2, parent, path4, path5);
		}
	} else {
		var details = {
			"path": path,
			"parent": parent,
			"fileName": data,
			"relativePath": relativePath + '/',
			"scope": scope
		};

		importScripts.push(details);
	}
}

gulp.task('frame:trim', function () {
	del([distPath + 'scripts/', distPath + 'styles/', distPath + '**/import.json']); // Delete existing build
});

gulp.task('frame:compile', ['frame:copy', 'frame:sass', 'frame:scripts', 'frame:fonts', 'frame:libscripts', 'frame:libcss']);

gulp.task('frame:watchCompile', function () {
	gulp.watch(['./global/**/*','./projects/photine/**/*'], ['frame:compile']);
});
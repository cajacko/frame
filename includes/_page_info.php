<?php

class Page {
	public function __construct($options = false){
		if($options) {
			$this->set_page_vars($options);
		}

		$this->head = array(
			'viewport' 				=> '<meta name="viewport" content="width=device-width, initial-scale=1">',
			'library_stylesheet' 	=> '<link rel="stylesheet" media="screen" href="/styles/lib.min.css">',
			'stylesheet' 			=> '<link rel="stylesheet" media="screen" href="/styles/style.min.css">',
		);

		if(isset($this->props['page_title'])) {
			$title = $this->props['page_title'];

			if(isset($this->props['title_postfix'])) {
				$title .= ' | '.$this->props['title_postfix'];
			}

			$this->head['title'] = '<title>'.$title.'</title>';
		}

		$this->scripts = array(
			'library_script' 	=> '<script src="/scripts/lib.min.js"></script>',
			'script' 			=> '<script src="/scripts/script.min.js"></script>',
		);

		if(DEV_ENVIRONMENT) {
			$this->head['library_stylesheet'] = '<link rel="stylesheet" media="screen" href="/styles/lib.css">';
			$this->head['stylesheet'] = '<link rel="stylesheet" media="screen" href="/styles/style.css">';

			$this->scripts['library_script'] = '<script src="/scripts/lib.js"></script>';
			$this->scripts['script'] = '<script src="/scripts/script.js"></script>';
		}

		$this->classes = array();

		if(isset($this->props['page_slug'])) {
			$this->classes[] = 'page' . $this->props['page_slug'];
		}

	}

	public function set_page_vars($options) {
		foreach($options as $name => $value) {
			$this->props[$name] = $value;
		}
	}

	public function print_head() {
		foreach($this->head as $head_item) {
			echo $head_item;
		}
	}

	public function print_scripts() {
		foreach($this->scripts as $script_item) {
			echo $script_item;
		}
	}
}
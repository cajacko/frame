<?php

function main_classes($frame) {
	$classes = '';

	if($frame || CONNECTED_TO_FACEBOOK) {
		$classes .= 'loggedIn ';
	} else {
		$classes .= 'loggedOut ';
	}

	if(!$frame) {
		$photo_count = count_user_photos();

		if(!$photo_count) {
			$classes .= 'noPhotos ';
		}
	}

	if('' != $classes) {
		echo 'class="'.trim($classes).'"';
	}

	return false;
}

function slideshow($frame = false) {
	
	?>

	<main id="slideshow" <?php main_classes($frame); ?>>
		<?php if(!$frame): ?>
			<?php $load_image = get_slideshow_items(true); ?>
			<img id="loadImage" src="<?php echo $load_image['url']; ?>" width="<?php echo $load_image['width']; ?>" height="<?php echo $load_image['height']; ?>">
		<?php endif; ?>

		<div id="startup">
			<h1>Photine</h1>
		</div>

		<?php if(!$frame): ?>
			<section id="account">
				<?php if(CONNECTED_TO_FACEBOOK): ?>
					<a id="settings"><i class="fa fa-cog"></i></a>
				<?php endif; ?>

				<div id="accountMenu">
					<a href="/logout">Logout</a>
					<p class="facebookStatus">
						<?php if(CONNECTED_TO_FACEBOOK): ?>
							You are currently connected to Facebook as: <?php echo FACEBOOK_NAME; ?>
						<?php else: ?>
							<button class="facebookLogin">Link account to Facebook</button>
						<?php endif; ?>
					</p>
					<?php if(CONNECTED_TO_FACEBOOK): ?>
						<p>You currently have: <span class="photoCount"><?php echo count_user_photos(); ?></span> photos.</p>
						<button class="processPhotos">Scan for photos</button>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
	</main>

	<?php
}
<?php

/*
*********************************************************
* INITIALISE MySQL INSTANCE								*
*********************************************************
*/
	/**
	 * These are the database login details
	 */
	if(file_exists('../dev_config.json')) {
		$config = json_decode(file_get_contents('../dev_config.json'), true);
	} else {
		$config = array(
			'host' => 'localhost',
			'user' => 'root',
			'password' => 'password',
			'database' => 'photine',
		);
	}

	define("HOST", $config['host']); // The host you want to connect to.
	define("USER", $config['user']); // The database username. 
	define("PASSWORD", $config['password']); // The database password. 
	//define("DATABASE", $config['database']); // The database name.

	/**
	 * Connect to the database
	 */
	$mysqli = new mysqli( HOST, USER, PASSWORD );

	$result = $mysqli->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'photine';");

	if($result->num_rows) {
		$mysqli->select_db($config['database']);
	} else {
		$mysqli->query("CREATE DATABASE IF NOT EXISTS `photine` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;");
		$mysqli->select_db($config['database']);
		$mysqli->query("
			CREATE TABLE `local_media` (
			  `id` int(11) NOT NULL,
			  `media_id` int(11) NOT NULL,
			  `width` int(11) NOT NULL,
			  `height` int(11) NOT NULL,
			  `size` int(11) NOT NULL DEFAULT '0'
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$mysqli->query("
			CREATE TABLE `options` (
			  `id` int(11) NOT NULL,
			  `options` varchar(32) NOT NULL,
			  `value` varchar(256) NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$mysqli->query("
			INSERT INTO `options` (`id`, `options`, `value`) VALUES
			(1, 'username', 'cajacko'),
			(2, 'email', 'charlie@charliejackson.com'),
			(5, 'password', 'Cajacko9'),
			(6, 'frame_id', 'fo8ybo4387tfdol98ybo874tb');
		");
		$mysqli->query("
			ALTER TABLE `local_media`
			  ADD PRIMARY KEY (`id`),
			  ADD UNIQUE KEY `media_id` (`media_id`);
		");
		$mysqli->query("
			ALTER TABLE `options`
			  ADD PRIMARY KEY (`id`),
			  ADD UNIQUE KEY `options` (`options`);
		");
		$mysqli->query("
			ALTER TABLE `local_media`
			  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
		");
		$mysqli->query("
			ALTER TABLE `options`
 			  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
		");

		// if(isset($_GET['attempt']) && is_numeric($_GET['attempt'])) {
		// 	$attempt = $_GET['attempt'];
		// } else {
		// 	$attempt = 0;
		// }

		// if($attempt > 5) {
		// 	echo 'Error: couldn\'t set up database';
		// } else {
		// 	header('Location: /?attempt='.$attempt);
		// }
	}
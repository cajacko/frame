<?php

function get_slideshow_items() {

	if(isset($_GET['type']) && 'local' == $_GET['type']) {
		get_local_slideshow_items();
		return false;
	}

	global $mysqli;

	$query = "SELECT * FROM options";
	$options = array();

	if ($result = $mysqli->query($query)) {

		/* fetch associative array */
		while ($row = $result->fetch_assoc()) {
			$options[$row['options']] = $row['value'];
		}

		/* free result set */
		$result->free();
	}

	$url = 'http://photine.life/action/slideshow-items/?';

	foreach($_GET as $key => $value) {
		if('url' != $key) {
			$url .= $key.'='.$value.'&';
		}	
	}

	$fields = array(
		'frame_id' => $options['frame_id'],
		'secret' => $options['secret'],
		'action' => 'api'
	);

	$fields_string = '';

	//url-ify the data for the POST
	foreach($fields as $key=>$value) { 
		$fields_string .= $key.'='.$value.'&'; 
	}

	rtrim($fields_string, '&');

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	//execute post
	$result = curl_exec($ch);

	if(curl_errno($ch)) {
		curl_close($ch);
		get_local_slideshow_items();
		return;
	}

	/* close connection */
	$mysqli->close();

	//close connection
	curl_close($ch);
	return;
}

function get_local_slideshow_items() {
	global $mysqli;

	$stmt = $mysqli->prepare("
		SELECT *
		FROM local_media
		ORDER BY RAND()
		LIMIT ?;
	");

	$stmt->bind_param("i", $limit);

	if( isset( $_GET[ 'limit' ] ) && is_numeric( $_GET[ 'limit' ] ) ) {
		$limit = $_GET[ 'limit' ];
	} else {
		$limit = 10;
	}

	$stmt->execute();

	$result = $stmt->get_result();

	if($result->num_rows) {
		while ($row = $result->fetch_assoc()) {
			//echo '<img src="'.$row['url'].'" height="200">';

			$image_array[] = array(
				'url' => '/media/' . $row['media_id'],
				'width' => $row['width'],
				'height' => $row['height'],
			);

		}

		echo json_encode($image_array);
	} else {
		echo json_encode(array('error' => 'no images found'));
	}

	$stmt->close();
}
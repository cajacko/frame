<?php

function get_global_photo_list($limit = 50) {
	global $mysqli;

	$query = "SELECT * FROM options";
	$options = array();

	if ($result = $mysqli->query($query)) {

		/* fetch associative array */
		while ($row = $result->fetch_assoc()) {
			$options[$row['options']] = $row['value'];
		}

		/* free result set */
		$result->free();
	}

	$url = 'http://photine.life/action/slideshow-items/?limit='.$limit;

	$fields = array(
		'username' => $options['username'],
		'email' => $options['email'],
		'password' => $options['password'],
		'frame_id' => $options['frame_id'],
		'action' => 'api'
	);

	$fields_string = '';

	//url-ify the data for the POST
	foreach($fields as $key=>$value) { 
		$fields_string .= $key.'='.$value.'&'; 
	}

	rtrim($fields_string, '&');

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds

	//execute post
	return curl_exec($ch);
}

function save_photos() {
	$list = get_global_photo_list();

	if($list) {
		$list = json_decode($list, true);

		set_time_limit(0);

		foreach($list as $media) {
			$local_url = '../public/local/'.$media[ 'id' ].'.jpg';
			// Is image stored locally
			if(!file_exists($local_url)) {
				save_photo($media);
			} 	
		}
	}
}

function save_photo($media) {
	global $mysqli;

	$query = "SELECT * FROM options";
	$options = array();

	if ($result = $mysqli->query($query)) {

		/* fetch associative array */
		while ($row = $result->fetch_assoc()) {
			$options[$row['options']] = $row['value'];
		}

		/* free result set */
		$result->free();
	}

	$url = 'http://photine.life/media/'.$media['id'];

	$fields = array(
		'username' => $options['username'],
		'email' => $options['email'],
		'password' => $options['password'],
		'frame_id' => $options['frame_id'],
		'action' => 'api'
	);

	$fields_string = '';

	//url-ify the data for the POST
	foreach($fields as $key=>$value) { 
		$fields_string .= $key.'='.$value.'&'; 
	}

	rtrim($fields_string, '&');

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	//execute post
	$result = curl_exec($ch);

	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds

	//execute post
	$result = curl_exec($ch);

	if(curl_errno($ch)) {	
		echo 'Curl error: ' . curl_error($ch);
		curl_close($ch);
		return;
	}

	//close connection
	curl_close($ch);

	$dir = '../public/local/';

	if (!file_exists($dir)) {
	    mkdir($dir, 0777, true);
	}

	$output = '../public/local/'.$media['id'].'.jpg';
	if(file_put_contents($output, $result)) {

		$stmt = $mysqli->prepare("
			INSERT INTO local_media (media_id, width, height, size)
			VALUES (?, ?, ?, ?);
		");

		$stmt->bind_param("iiii", $media_id, $width, $height, $size);
		$media_id = $media['id'];
		$width = $media['width'];
		$height = $media['height'];
		$size = filesize($output);
		$stmt->execute();

		echo 'added: '.$output.'<br>';
	}
}

function delete_media() {

	if(isset($_GET['delete']) && is_numeric($_GET['delete'])) {
		global $mysqli;

		$stmt = $mysqli->prepare("
			DELETE FROM local_media
			WHERE media_id = ?
		");

		$stmt->bind_param("i", $media_id);
		$media_id = $_GET['delete'];
		$stmt->execute();

		$file = '../public/local/'.$_GET['delete'].'.jpg';

		if (file_exists($file)) {
		    unlink($file);
		}
	}
}

function delete_all() {
	$dir = '../public/local/';
	$files = scandir('../public/local/');
	
	foreach($files as $file) {
		if($file != '.' || $file != '..') {
			unlink($dir.$file);
		}
	}

	global $mysqli;

	$stmt = $mysqli->prepare("
		DELETE FROM local_media
	");

	$stmt->execute();
}

function local_media_size() {
	global $mysqli;

	$stmt = $mysqli->prepare("
		SELECT size
		FROM local_media
	");

	$stmt->execute();

	$stmt->bind_result($size);
	$total_size = 0;

    /* fetch values */
    while ($stmt->fetch()) {
        $total_size = $total_size + $size;
    }

    return $total_size;
}
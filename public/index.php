<?php

// Enable debugging
error_reporting(E_ALL);
ini_set('display_errors', true);

define('DEV_ENVIRONMENT', TRUE);

if( isset( $_GET[ 'url' ] ) ) {
	$request = $_GET[ 'url' ];
	$request = explode( '/', $request );
	$request_array = array();

	foreach( $request as $value ) {
		$request_array[] = strtolower( $value );
	}

	switch($request_array[ 0 ]) {
		case 'media':
			if( isset( $request_array[ 1 ] ) && is_numeric( $request_array[ 1 ] ) ) {
				$_GET[ 'id' ] = $request_array[ 1 ];
			}

			require('../page_types/img.php');
			exit;

		case 'action':
			$_GET[ 'action' ] =  $request_array[ 1 ];
			require( '../functions/action.php' );
			exit;
	}
}

require('../page_types/home.php');
exit;
?>
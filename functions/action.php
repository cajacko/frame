<?php 
require_once( '../functions/import.php' );

switch($_GET['action']) {
	case 'slideshow-items':
		get_slideshow_items();
		break;

	case 'save-media':
		save_photos();
		break;

	case 'delete-media':
		delete_media();
		break;

	case 'delete-all':
		delete_all();
		break;

	default:
		echo 'Action not found';
}

exit;
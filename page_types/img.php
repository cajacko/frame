<?php
/*
*********************************************************
* DISPLAY MEDIA                                         *
*********************************************************
*/
	require_once( '../functions/import.php' ); // Import required functions

	/*
	*********************************************************
	* REGISTER PAGE FUNCTIONS                               *
	*********************************************************
	*/
		/**
		 * Makes sure an image is always returned. Used if 
		 * the requested image can't be shown. It display 
		 * and image with a custom error message.
		 */
		function echo_error_image( $text ) {
			echo $text;
			exit;
			header("HTTP/1.0 404 Not Found");
			exit;
			echo file_get_contents( 'http://placehold.it/1000x750?text=' . urlencode( $text ) ); // Get and return the image
		}

		function get_media_content($id) {
			global $mysqli;

			$query = "SELECT * FROM options";
			$options = array();

			if ($result = $mysqli->query($query)) {

				/* fetch associative array */
				while ($row = $result->fetch_assoc()) {
					$options[$row['options']] = $row['value'];
				}

				/* free result set */
				$result->free();
			}

			/* close connection */
			$mysqli->close();

			$url = 'http://photine.life/media/'.$id.'?frame_width=' . $_GET['frame_width'] . '&frame_height=' . $_GET['frame_height'];

			$fields = array(
				'frame_id' => $options['frame_id'],
				'secret' => $options['secret'],
				'action' => 'api'
			);

			$fields_string = '';

			//url-ify the data for the POST
			foreach($fields as $key=>$value) { 
				$fields_string .= $key.'='.$value.'&'; 
			}

			rtrim($fields_string, '&');

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 20); //timeout in seconds

			//execute post
			$result = curl_exec($ch);

			echo ($result);

			if($errno = curl_errno($ch)) {
			    $error_message = curl_strerror($errno);
			    echo "cURL error ({$errno}):\n {$error_message}";

			    echo_error_image( 'Timeout' ); // Return error image
				return;
			}

			//close connection
			curl_close($ch);

			return $result;
		}

	/*
	*********************************************************
	* PROCESS                                               *
	*********************************************************
	*/
	header( 'Content-Type: ' . 'image/jpeg' ); // Set header

	// Is a media ID set and valid?
	if( isset( $_GET[ 'id' ] ) && is_numeric( $_GET[ 'id' ] ) ) { 
		$local_url = '../public/local/'.$_GET[ 'id' ].'.jpg';
		// Is image stored locally
		if(file_exists($local_url)) {
			echo file_get_contents($local_url);
		} 
		// Image is not stored locally, get from the web
		else {
			echo get_media_content($_GET[ 'id' ]);
		}
	} else {
		echo_error_image( 'Incorrect image URL' ); // Return error image
	}
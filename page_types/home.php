<?php
require('../functions/import.php');

define("CONNECTED_TO_FACEBOOK", false);
define("LOGGED_IN", true);

$page = new Page();

?>

<!DOCTYPE html>
<html lang="en-GB" class="no-js">
	<head>
		<?php $page->print_head(); ?>
	</head>
	<body>	
		<?php slideshow(true); ?>

		<?php $page->print_scripts(); ?>
	</body>
</html>
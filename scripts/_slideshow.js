var slideshowLogAll = true;

var initialImagesToLoad = 2;
var maxImagesToLoadAtATime = 5;
var maxImagesInQueue = 20;
var maxPrevImages = 20;
var maxTimeToLoadImage = 20;
var slideDuration = 10;
var loadTimeout = 20;
var shownFirstImage = Date.now();
var gettingSlideshowListing = false;

$(document).ready(function() {
	if($('#slideshow').length && $('#slideshow').hasClass('loggedIn')) {
		init();

		$('#loadImage').load(function() {
			shownFirstImage = Date.now();

			$(this).css(returnImgPosition($(this).attr('width'), $(this).attr('height')));

			if(slideshowLogAll) {
				console.log('#loadImage has loaded, show');
			}

			fadeOutOldContent();
		}).on('error', function() {
			if(slideshowLogAll) {
				console.log('#loadImage has an error, deleting');

				$(this).remove();
			}
		});

		$(window).resize(function() {
			if(slideshowLogAll) {
				console.log('Resize window');
			}

			windowWidth = $( window ).width();
			windowHeight = $( window ).height();

			$('img').each(function() {
				$(this).css(returnImgPosition($(this).attr('width'), $(this).attr('height')));
			});

			var count = 0;

			$(nextImages).each(function() {
				$(nextImages[count].img).css(returnImgPosition(nextImages[count].width, nextImages[count].height));
				count++;
			});
		});
	}
});

var nextImages = [];
var prevImages = [];
var loadingImages = [];
var imageQueue = [];
var slideShowStarted = false;

var windowWidth = $( window ).width();
var windowHeight = $( window ).height();

function init() {
	if(slideshowLogAll) {
		console.log('Start');
		console.log('windowWidth: ' + windowWidth + ' windowHeight: ' + windowHeight);
	}

	updateImageListAndConfig(maxImagesInQueue);
}

function updateImageListAndConfig(numberOfItems) {
	var url = "/action/slideshow-items/?limit=" + numberOfItems + '&display_width=' + windowWidth + '&display_height=' + windowHeight;

	if(slideshowLogAll) {
		console.log('Get: ' + url);
	}

	gettingSlideshowListing = true;

	$.ajax({
		url: url,
		dataType: "json",
		crossDomain: true,
	}).done(function( data ) {
		gettingSlideshowListing = false;

		if(slideshowLogAll) {
			console.log('Got data, length: ' + data.length + ':');
			console.log(data);
		}

		$.each(data, function(index, row) {
			row.url = row.url + '?frame_width=' + windowWidth + '&frame_height=' + windowHeight;
			imageQueue.push( row );
		});

		if(slideshowLogAll) {
			console.log('imageQueue, length: ' + imageQueue.length + ':');
			console.log(imageQueue);
		}

		preLoadImages();
	});
}

function preLoadImages() {
	$( imageQueue ).each( function() {
		loadingImages.push(this);
		imageQueue.splice( 0, 1 );	

		if(slideshowLogAll) {
			console.log('loading:');
			console.log(this);
		}

		loadImage(this);

		if((slideShowStarted && loadingImages.length >= maxImagesToLoadAtATime) || (!slideShowStarted && loadingImages.length >= initialImagesToLoad)) {
			return false;
		}
	});

	if(slideshowLogAll) {
		console.log('LoadingImages, length: ' + loadingImages.length + ':');
		console.log(loadingImages);

		console.log('imageQueue, length: ' + imageQueue.length + ':');
		console.log(imageQueue);
	}

	if(!slideShowStarted && loadingImages.length < initialImagesToLoad) {
		if(slideshowLogAll) {
			console.log('Slideshow hasn\'t started and there aren\'t enough pending images. Loading more.');
		}

		updateImageListAndConfig(maxImagesInQueue);
	}
}

function loadImage( object ) {
	var src = object[ 'url' ];

	if(slideshowLogAll) {
		console.log('Loading: ' + src);
	}

	setTimeout(function() {
		removeImageFromLoading(object[ 'url' ], 'Image taking too long to load and has been removed: ' + src + ' loadingImages, length: ' + loadingImages.length + ': ');

		var count = 0;
		$(loadingImages).each( function() {

			if( this[ 'url' ] == object[ 'url' ] ) {
				loadingImages.splice( count, 1 );

				if(slideshowLogAll) {
					console.log('Image taking too long to load and has been removed: ' + src + ' loadingImages, length: ' + loadingImages.length + ': ');
					console.log(loadingImages);
				}
				return;
			}
			count++;
		});
		
	}, loadTimeout * 1000);

	$('<img src="' + src + '">').load(function() {

		if(slideshowLogAll) {
			console.log('Loaded: ' + src);
		}

		var element = $( this );
		var width = object[ 'width' ];
		var height = object[ 'height' ];	

		$(element).css(returnImgPosition(width, height)).attr('width', width).attr('height', height);

		object.img = $(element);
		nextImages.push(object);

		if(slideshowLogAll) {
			console.log('nextImages, length: ' + nextImages.length + ':');
			console.log(nextImages);
		}

		if(!slideShowStarted) {
			if(slideshowLogAll) {
				console.log('Starting slideshow as hasn\'t begun yet');
			}

			imageLoopTimeout();
			slideShowStarted = true;
		}

		removeImageFromLoading(object[ 'url' ], false);

		if(slideshowLogAll) {
			console.log('loadingImages, length: ' + loadingImages.length + ':');
			console.log(loadingImages);
		}
	}).on('error', function() {
		removeImageFromLoading(object[ 'url' ], false);

		if(slideshowLogAll) {
			console.log('Image error, removing from loadingImages:' + src + ', length: ' + loadingImages.length + ': ');
			console.log(loadingImages);
		}
	});
}

function removeImageFromLoading(url, log) {
	var count = 0;
	$(loadingImages).each( function() {

		if( this[ 'url' ] == url ) {
			loadingImages.splice( count, 1 );

			if(slideshowLogAll && log) {
				console.log(log);
				console.log(loadingImages);
			}
			return;
		}
		count++;
	});

	if(!slideShowStarted && loadingImages.length < initialImagesToLoad) {
		if(slideshowLogAll) {
			console.log('Slideshow hasn\'t started and there aren\'t enough loading images. Loading more.');
		}

		preLoadImages();
	} else if(loadingImages.length < maxImagesToLoadAtATime) {
		if(slideshowLogAll) {
			console.log('There is now space to load more images');
		}

		preLoadImages();
	}
}

function returnImgPosition( width, height ) {
	if(slideshowLogAll) {
		console.log('Getting position, width: ' + width + ' and height: ' + height);
	}

	var windowRatio = windowWidth / windowHeight;
	var imgRatio = width / height;

	if( windowRatio > imgRatio ) {
		var imgWidth = windowWidth;
		var imgHeight = ( height * imgWidth ) / width;
		var left = 0;
		var top = -((imgHeight - windowHeight) / 2);
	} else {
		var imgHeight = windowHeight;
		var imgWidth = ( width * imgHeight ) / height;
		var top = 0;
		var left = -((imgWidth - windowWidth) / 2);
	}

	var position = {'margin-left': left, 'margin-top': top, 'height': imgHeight, 'width': imgWidth};

	if(slideshowLogAll) {
		console.log('windowRatio: ' + windowRatio + ' imgRatio: ' + imgRatio + ' newWidth: ' + imgWidth + ' newHeight: ' + imgHeight + ' top: ' + top + ' left: ' + left);
	}

	return position;
}

function imageLoopTimeout() {
	var time = Date.now();
	var dif = (time - shownFirstImage) / 1000;
	
	if(dif > slideDuration) {
		if(slideshowLogAll) {
			console.log('imageLoopTimeout: loadImage shown for' + dif + ' seconds, loading next image now');
		}

		imageLoop();
	} else {
		var timeout = (slideDuration - dif)  * 1000;

		if(slideshowLogAll) {
			console.log('imageLoopTimeout: loadImage shown for' + dif + ' seconds, waiting for ' + timeout + ' seconds before continuing');
		}

		setTimeout(function() {
			imageLoop();
		}, timeout);
	}
}

function imageLoop() {
	pushNextImage(nextImages[0]);
	setInterval( function() { 
		if(nextImages.length == 0) {
			if(slideshowLogAll) {
				console.log('Tried to push an image but there are none in the queue. Loading more.');
			}

			getMoreImages();
		} else {
			pushNextImage(nextImages[0]);
		}	
	}, slideDuration * 1000 );
}

function pushNextImage( object ) {
	if(slideshowLogAll) {
		console.log('pushing:');
		console.log(object);
	}

	fadeOutOldContent();

	$( '#slideshow' ).prepend( object[ 'img' ][ 0 ] );
	nextImages.splice( 0, 1 );

	if(slideshowLogAll) {
		console.log('nextImages, length: ' + nextImages.length + ':');
		console.log(nextImages);
	}	

	getMoreImages();
}

function fadeOutOldContent() {
	if($('#startup').length) {
		var selector = '#startup';
	} else {
		var selector = 'img';
	}

	$( selector ).remove(); // As the frames can't do animations yet

	// $( selector ).fadeOut('slow', function() {
	// 	$( this ).remove();
	// });
}

function getMoreImages() {
	if( imageQueue.length < maxImagesInQueue ) {

		var limit = maxImagesInQueue - imageQueue.length;

		// if(limit > maxImagesToLoadAtATime) {
		// 	limit = maxImagesToLoadAtATime;
		// }

		updateImageListAndConfig( limit );
	}
}